import random

constHelp = """
help - напечатать справку по программе
add - добавить задачу в список дел (название запрашивается у пользователя)
show - вывести список задач на дату
show_all - вывести список всех задач

"""

constRandomTasks = ["Помыть машину", "Помыть посуду",
                    "Приготовить поесть", "Покормить кота", "Прогулка"]

constMyName = "Дмитрий"


def addTaskToDict(task, date, dict):
    if date in dict:
        dict[date].append(task)
    else:
        dict[date] = []
        dict[date].append(task)

    print(f"Задача {task} добавлена на дату {date}")
    return


def checkName(string):
    if constMyName in string:
        return True
    else:
        return False


tasks = {}

isRun = True

while isRun:

    command = input("Введите команду: ")

    if (command == "help"):
        print(constHelp)

    elif checkName(command):
        print("Ба! Знакомые все лица!")

    elif (command == "add"):
        task = input("Введите задачу: ")
        date = input("Введите дату для задачи: ")

        addTaskToDict(task, date, tasks)

    elif (command == "show"):
        date = input("Введите дату для показа списка задач: ")
        if date in tasks:
            for task in tasks[date]:
                print(f"- {task}")
        else:
            print("На такую дату нет задач")

    elif (command == "show_all"):
        if (len(tasks) > 0):
            for date in tasks:
                print(f"Дата: {date}")
                for task in tasks[date]:
                    print(f"- {task}")
        else:
            print("Список задач пустой.")

    elif (command == "random"):
        task = random.choice(constRandomTasks)
        addTaskToDict(task, "Сегодня", tasks)

    elif (command == "exit"):
        print("Спасибо за использование! До свидания!")
        isRun = False

    else:
        print("Команда не определена, воспользуйтесь командой 'help'")
